## Robot suiveur de ligne [prototype 1]

![alt tag](https://framagit.org/RobotsLibres/images/raw/master/001.png)

Dossier "STL" tous les fichiers en téléchargement pour l'impression 3d du robot.

Dossier "STEP" le fichier en téléchargement pour visulaiser le robot. (standard pour tous les logiciels de CAO)